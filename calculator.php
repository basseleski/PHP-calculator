<?php
class Calc {
    public $operator;
    public $firstNumber;
    public $secondNumber;

    public function calculate() {
        if ($this->operator == "addition"){
            echo "uw nummer is " , $this->firstNumber + $this->secondNumber;
        }
        else if($this->operator == "subtraction") {
            echo "uw nummer is ",$this->firstNumber - $this->secondNumber;
        }
        else if($this->operator == "multiplication") {
            echo "uw nummer is ",$this->firstNumber * $this->secondNumber;
        }
        else if($this->operator == "division") {
            echo "uw nummer is ",$this->firstNumber / $this->secondNumber;
        }
    }
    public function setOperator($argument = NULL) {
        $valid = false;
        $input = $argument;
        if (!isset($argument)) {
            echo "wil je optellen, aftrekken, vermenigvuldigen of delen?" . PHP_EOL;
        }
        while (!$valid) {
            if (!isset($input)) {
                $input = readline();
            }
            if ($input == "optellen" || $input == "+"){
                $this->operator = "addition";
                $valid = true;
            }
            else if($input == "aftrekken" || $input == "-") {
                $this->operator = "subtraction";
                $valid = true;
            }
            else if($input == "verminigvuldigen" || $input == "*" || $input == "x") {
                $this->operator = "multiplication";
                $valid = true;
            }
            else if($input == "delen" || $input == "/") {
                $this->operator = "division";
                $valid = true;
            }
            else {
                $input = null;
                echo "de operator wordt niet herkent".PHP_EOL;
            }
        }
    }
    public function setFirstNumber($argument = NULL) {
        $valid = false;
        $input = $argument;
        if (!isset($argument)) {
            echo "voer je eerste nummer in".PHP_EOL;
        }
        while (!$valid) {
            if (!isset($input)) {
                $input = readline();
            }
            if (is_numeric($input)) {
                $this->firstNumber = $input;
                $valid = true;
            }
            else {
                $input = null;
                echo "dit is geen getal".PHP_EOL;
            }
        }
    }
    public function setSecondNumber($argument = NULL) {
        $valid = false;
        $input = $argument;
        if (!isset($argument)) {
            echo "voer je tweede nummer in".PHP_EOL;
        }
        while (!$valid) {
            if (!isset($input)) {
                $input = readline();
            }
            if (is_numeric($input)) {
                $this->secondNumber = $input;
                $valid = true;
            }
            else {
                $input = null;
                echo "dit is geen getal".PHP_EOL;
            }
        }
    }
}
$calc = new Calc();
if (isset($argv[4])) {
    echo "er zijn te veel argumenten";
    exit;
}
if(isset($argv[1])) {
    $calc->setFirstNumber($argv[1]);
} else {
    $calc->setFirstNumber();
}
if(isset($argv[2])) {
    $calc->setOperator($argv[2]);
} else {
    $calc->setOperator();
}
if(isset($argv[3])) {
    $calc->setSecondNumber($argv[3]);
} else {
    $calc->setSecondNumber();
}
$calc->calculate();




