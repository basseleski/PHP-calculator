<?php
class Calc {
    public $operator;
    public $firstNumber;
    public $secondNumber;

    public function calculate() {
        if ($this->operator == "addition"){
            echo "uw nummer is " , $this->firstNumber + $this->secondNumber,PHP_EOL;
        }
        else if($this->operator == "subtraction") {
            echo "uw nummer is ",$this->firstNumber - $this->secondNumber,PHP_EOL;
        }
        else if($this->operator == "multiplication") {
            echo "uw nummer is ",$this->firstNumber * $this->secondNumber,PHP_EOL;
        }
        else if($this->operator == "division") {
            echo "uw nummer is ",$this->firstNumber / $this->secondNumber,PHP_EOL;
        }
    }
    public function setOperator(): void
    {
        $valid = false;
        echo "wil je optellen, aftrekken, vermenigvuldigen of delen?" . PHP_EOL;
        while (!$valid) {
            $input = readline();

            if ($input == "optellen" || $input == "+"){
                $this->operator = "addition";
                $valid = true;
            }
            else if($input == "aftrekken" || $input == "-") {
                $this->operator = "subtraction";
                $valid = true;
            }
            else if($input == "verminigvuldigen" || $input == "*" || $input == "x") {
                $this->operator = "multiplication";
                $valid = true;
            }
            else if($input == "delen" || $input == "/") {
                $this->operator = "division";
                $valid = true;
            }
            else {
                echo "de operator wordt niet herkent".PHP_EOL;
            }
        }
    }
    public function setFirstNumber(): void
    {
        $valid = false;
        echo "voer je eerste nummer in".PHP_EOL;

        while (!$valid) {
            $input = readline();
            if (is_numeric($input)) {
                $this->firstNumber = $input;
                $valid = true;
            }
            else {
                echo "dit is geen getal".PHP_EOL;
            }
        }

    }
    public function setSecondNumber(): void
    {
        $valid = false;
        echo "voer je tweede nummer in".PHP_EOL;

        while (!$valid) {
            $input = readline();
            if (is_numeric($input)) {
                $this->secondNumber = $input;
                $valid = true;
            }
            else {
                echo "dit is geen getal".PHP_EOL;
            }
        }
    }

}

if (in_array($argv, "debug")) {
    echo var_dump($argv),PHP_EOL;
}

$calc = new Calc();
$calc->setOperator();
$calc->setFirstNumber();
$calc->setSecondNumber();
$calc->calculate();




